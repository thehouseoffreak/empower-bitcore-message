var bitcore = require('empower-bitcore-lib');
bitcore.Message = require('./lib/message');

module.exports = bitcore.Message;
